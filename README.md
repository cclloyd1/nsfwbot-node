# nsfwbot-node

This bot can help manage NSFW posts/subreddits on Reddit and had a Discord component to store posts for personal use.

# How to use

## Prerequisites

There are some things you should have for this to work properly.

- A discord bot account
- 2 reddit accounts
- A PeerTube instance that you have upload permission to, and replaced the `youtube-dl` executable on (optional)

### Steps to prepare
1. Get token for both reddit accounts
2. Ensure that you have a discord server set up solely for NSFW content
3. Add the bot to your discord server
4. In your PeerTube instance, go to the data folder.  There should be a `bin` folder with `youtube-dl` in it.  Rename that to `youtube-dl.bak`
5. Download the latest version of `yt-dlp` from [here](https://github.com/yt-dlp/yt-dlp/releases/latest/) (you want the linux executable).  Place that in the folder, rename it to `youtube-dl`, and give it execute permissions (`chmod +x youtube-dl`)
6. Run the bot

### Docker Compose

Here is an example docker-compose snippet.  **MAKE SURE TO ADD ALL REQUIRED ENVIRONMENT VARIABLES!!**
```yaml
---
version: "2.1"
services:
  nsfwbot:
    image: registry.gitlab.com/cclloyd1/nsfwbot-node:latest
    container_name: nsfwbot
    environment:
      - DISCORD_TOKEN=asdf
      - DISCORD_CLIENT_ID=asdf
    volumes:
      - /path/to/media:/watch/media
      - /path/to/storage:/source
    restart: unless-stopped
```

## Required Environment Variables
These variables are required for the bot to function properly.  It does not check for consistency in them, so it's up to you, the user, to make sure they're set properly!

| Variable                    | Description                          | Default               |
|:----------------------------|:-------------------------------------|:----------------------|
| `DISCORD_TOKEN`             | Token for discord bot account        | `none`                |
| `DISCORD_CLIENT_ID`         | ID of the bot user                   | `none`                |
| `DISCORD_GUILD_ID`          | ID of the guild for the bot          | `none`                |
| `REDDIT_MAIN_USERNAME`      | Username of main reddit account      | `none`                |
| `REDDIT_MAIN_CLIENT_ID`     | Client ID of main reddit account     | `none`                |
| `REDDIT_MAIN_CLIENT_SECRET` | Client secret of main reddit account | `none`                |
| `REDDIT_MAIN_TOKEN`         | Token for main reddit account        | `none`                |
| `REDDIT_ALT_USERNAME`       | Username of alt reddit account       | `none`                |
| `REDDIT_ALT_CLIENT_ID`      | Client ID of alt reddit account      | `none`                |
| `REDDIT_ALT_CLIENT_SECRET`  | Client secret of alt reddit account  | `none`                |
| `REDDIT_ALT_TOKEN`          | Token for main alt account           | `none`                |
| `PEERTUBE_HOST`             | Hostname of PeerTube instance        | `peertube.com`        |
| `PEERTUBE_USERNAME`         | Username for PeerTube account        | `none`                |
| `PEERTUBE_PASSWORD`         | Password for PeerTube account        | `none`                |
| `PEERTUBE_CHANNEL`          | Channel to post videos to            | `2` (default channel) |


# Optional Environment Variables
These are not required to run, but can alter the function of how the bot works.

| Variable        | Description                           | Default |
|:----------------|:--------------------------------------|:--------|
| `DRYRUN`        | Do not perform any actual operations. | `false` |
| `DISCORD_DEBUG` | Additional logging for bot            | `false` |
| `DEV`           | Switches to dev mode                  | `false` |


# TODO
- Add helpers for reddit tokens
- Clarify setup instructions
- More peertube options
- Better peertube reliability
