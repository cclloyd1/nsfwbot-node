FROM node:16
WORKDIR /app

RUN yarn global add reddit-oauth-helper

ADD package.json /app/package.json
RUN yarn install

RUN wget https://github.com/yt-dlp/yt-dlp/releases/download/2022.04.08/yt-dlp -O /app/yt-dlp && chmod +x /app/yt-dlp

ADD index.js /app/index.js
ADD bot /app/bot


ENTRYPOINT ["yarn", "start"]

