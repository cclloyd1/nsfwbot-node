import snoowrap from "snoowrap";
import {dotEnv} from './utils.js';


export const rn = new snoowrap({
    userAgent: 'nsfwbot',
    clientId: dotEnv.REDDIT_MAIN_CLIENT_ID,
    clientSecret: dotEnv.REDDIT_MAIN_CLIENT_SECRET,
    refreshToken: dotEnv.REDDIT_MAIN_TOKEN,
});

export const ra = new snoowrap({
    userAgent: 'nsfwbot',
    clientId: dotEnv.REDDIT_ALT_CLIENT_ID,
    clientSecret: dotEnv.REDDIT_ALT_CLIENT_SECRET,
    refreshToken: dotEnv.REDDIT_ALT_TOKEN,
});
