import {MessageEmbed} from 'discord.js';


export const embedColor = {
    info: 0x1976d2,
    success: 0x388e3c,
    warning: 0xf5c000,
    error: 0xd32f2f,
    running: 0x9e9e9e,
}

export const embedReply = {
    info: (msg) => new MessageEmbed().setColor(embedColor.info).setTitle(msg),
    success: (msg) => new MessageEmbed().setColor(embedColor.success).setTitle(msg),
    warning: (msg) => new MessageEmbed().setColor(embedColor.warning).setTitle(msg),
    error: (msg) => new MessageEmbed().setColor(embedColor.error).setTitle(msg),
    running: (msg) => new MessageEmbed().setColor(embedColor.running).setTitle(msg),
}

export const serviceStatus = {
    DISABLED: 0,
    STOPPED: 1,
    STARTING: 2,
    RUNNING: 3,
    STOPPING: 4,
}

export const serviceStatusName = (num) => {
    switch (num) {
        case serviceStatus.DISABLED: return 'disabled';
        case serviceStatus.STOPPED: return 'stopped';
        case serviceStatus.STARTING: return 'starting';
        case serviceStatus.RUNNING: return 'running';
        case serviceStatus.STOPPING: return 'stopping';
        default: return 'unknown';
    }
}
