import dayjs from 'dayjs';
import {dotEnv, logger, sleep} from './utils.js';
import {embedReply, serviceStatus, serviceStatusName} from './enums.js';
import duration from 'dayjs/plugin/duration.js';
dayjs.extend(duration)


export class BaseService {
    constructor(client, options={}) {
        this.client = client;
        this.name = options.name ?? 'default';

        this.enabled = options.enabled ?? false;
        this.notifications = options.interval ?? true;
        this.interval = dayjs.duration({minutes: options.interval ?? 30})

        this.status = serviceStatus.STOPPED;
        this.running = false;
        this.lastRun = null;
        this.lastFinish = null;
        this.successful = false;
        this.thread = null;

        client.services.set(this.name, this);
    }

    /**
     * Abstract method.  Reimplement actual logic here.
     */
    async handle() {}

    /**
     * Sends notifications to the designated channel, or replies to a supplied interaction, with the status of the service.
     * @param [interaction] Interaction to reply to
     * @returns {Promise<void>}
     */
    async notify(interaction) {
        const channel = await this.client.channels.fetch(dotEnv.DISCORD_STATUS_CHANNEL);
        const embed = embedReply.info(`Status for ${this.name} service`)
            .addField('Enabled', `${this.enabled}`, true)
            .addField('Status', `${serviceStatusName(this.status)}`, true)
            .addField('Last Run', this.lastRun ? `<t:${this.lastRun.unix()}:R>` : 'never', true)
            .addField('Last Completed', this.lastFinish ? `<t:${this.lastFinish.unix()}:R>` : 'never', true)
            .addField('Last Run Successful', `${this.successful}`, true)
        if (interaction) {
            if (interaction.replied || interaction.deferred)
                await interaction.editReply({embeds: [embed]})
            else
                await interaction.reply({embeds: [embed]})
        }
        else
            await channel.send({embeds: [embed]});
    }

    async start() {
        this.running = true;
        this.thread = this.run();
    }

    async restart() {
        // TODO: Implement restart method
    }

    async stop() {
        this.running = false;
        this.status = serviceStatus.STOPPING;
    }

    async enable() {
        this.enabled = true;
    }

    async disable() {
        this.enabled = false;
    }

    async run() {
        while (this.running) {
            logger.info(`Running ${this.name} service`);

            // Reset run info
            this.lastRun = dayjs();
            this.successful = false;
            this.status = serviceStatus.RUNNING;

            // Actually run the routine
            try {
                await this.handle();
                this.lastFinish = dayjs();
                this.successful = true;
            } catch (error) {
                logger.error(`Encountered error running ${this.name} service.`);
                logger.error(error);
            }

            // Send notifications
            if (this.notifications && dotEnv.DISCORD_STATUS_CHANNEL) {
                try {
                    await this.notify();
                } catch (error) {
                    logger.error(`Error posting notification for ${this.name} service.`)
                }
            }

            // Sleep routine
            logger.info(`Finished ${this.name} service.  Will run again in ${this.interval.asMinutes()} minutes.`);
            await sleep(this.interval.asMilliseconds());
            // TODO: change long sleep to series of short sleeps that checks if its enabled or not
        }

        logger.info(`Exiting ${this.name} service`);
        this.status = serviceStatus.STOPPED;
    }
}

