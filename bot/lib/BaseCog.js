

class BaseCog {
    constructor(client) {
        this.client = client;
    }

    /**
     * This is meant to be overridden by the child class.  Register all events here, as this method will be called when you add the cog to the bot startup.
     * EX: this.client.on('interactionCreate', (...args) => interactionCreate(...args));
     */
    registerEvents = () => {
        throw 'You have to override this register events function';
    }

    /**
     * This is meant to be overridden by the child class.  Register all commands here, as this method will be called when you add the cog to the bot startup.
     * EX: this.client.commands.set('example', exampleCommand);
     */
    registerCommands = () => {
        throw 'You have to override this register commands function';
    }
}

export { BaseCog };

