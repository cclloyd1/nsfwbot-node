import {config as dotenvConfig} from 'dotenv';
import axiosDefault from 'axios';
import {Routes} from 'discord-api-types/v9';
import {REST} from '@discordjs/rest';
import snoowrap from 'snoowrap';
import fs from 'fs';
import path from 'path';
import pino from 'pino';
import pretty, {colorizerFactory} from 'pino-pretty';
import dayjs from 'dayjs';
import {embedReply} from './enums.js';


dotenvConfig();


export const dotEnv = {
    DISCORD_DEBUG: process.env.DISCORD_DEBUG === 'true',
    DISCORD_STATUS_CHANNEL: process.env.DISCORD_STATUS_CHANNEL ?? null,
    DEV: process.env.DEV === 'true',
    DRYRUN: process.env.DRYRUN === 'true',
    DISCORD_TOKEN: process.env.DISCORD_TOKEN ?? null,
    DISCORD_CLIENT_ID: process.env.DISCORD_CLIENTID ?? null,
    DISCORD_GUILD_ID: process.env.DISCORD_GUILDID ?? null,
    REDDIT_MAIN_USERNAME: process.env.REDDIT_MAIN_USERNAME ?? null,
    REDDIT_MAIN_CLIENT_ID: process.env.REDDIT_MAIN_CLIENT_ID ?? null,
    REDDIT_MAIN_CLIENT_SECRET: process.env.REDDIT_MAIN_CLIENT_SECRET ?? null,
    REDDIT_MAIN_TOKEN: process.env.REDDIT_MAIN_TOKEN ?? null,
    REDDIT_ALT_USERNAME: process.env.REDDIT_ALT_USERNAME ?? null,
    REDDIT_ALT_CLIENT_ID: process.env.REDDIT_ALT_CLIENT_ID ?? null,
    REDDIT_ALT_CLIENT_SECRET: process.env.REDDIT_ALT_CLIENT_SECRET ?? null,
    REDDIT_ALT_TOKEN: process.env.REDDIT_ALT_TOKEN ?? null,
    PEERTUBE_HOST: process.env.PEERTUBE_HOST ?? null,
    PEERTUBE_USERNAME: process.env.PEERTUBE_USERNAME ?? null,
    PEERTUBE_PASSWORD: process.env.PEERTUBE_PASSWORD ?? null,
    PEERTUBE_CHANNEL: process.env.PEERTUBE_CHANNEL ?? 2,
    REDDIT_SERVICE: process.env.REDDIT_SERVICE !== 'false',
    YTDLP_PATH: process.env.YTDLP_PATH ?? '/app/yt-dlp',
}


const levelColorize = colorizerFactory(true)
const stream = pretty({
    colorize: true,
    ignore: 'pid,hostname',
    //messageFormat: '{levelLabel} - url:{msg}',

    customPrettifiers: {
        // The argument for this function will be the same
        // string that's at the start of the log-line by default:
        //time: timestamp => `🕰 ${timestamp}`,
        time: timestamp => dayjs.unix(timestamp / 1000).format(dotEnv.DISCORD_DEBUG ? 'HH:mm:ss' : 'YYYY-MM-DD HH:mm:ssZ[Z]'),
        level: logLevel => `${levelColorize(logLevel)}`.padEnd(16),
        //level: logLevel => `${levelColorize(logLevel)}`,
        // The argument for the level-prettifier may vary depending
        // on if the levelKey option is used or not.
        // By default this will be the same numerics as the Pino default:
        //level: logLevel => `LEVEL: ${logLevel}`
        // other prettifiers can be used for the other keys if needed, for example
        //hostname: hostname => colorGreen(hostname)
        //pid: pid => colorRed(hostname)
        //name: name => colorBlue(name)
        //caller: caller => colorCyan(caller)
    }
})
export const logger = pino(stream);
logger.info(dotEnv);


export const sleep = (ms) =>  {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Return null if any of the vars are undefined
export const reddit = [
    process.env.REDDIT_CLIENT_ID,
    process.env.REDDIT_CLIENT_SECRET,
    process.env.REDDIT_TOKEN,
].includes(undefined) ?
    null :
    new snoowrap({
        userAgent: 'nsfwbot',
        clientId: process.env.REDDIT_CLIENT_ID,
        clientSecret: process.env.REDDIT_CLIENT_SECRET,
        refreshToken: process.env.REDDIT_TOKEN,
    });


export const deployCommands = (client) => {
    const commands = [];
    for (const [, c] of client.commands)
        commands.push(c.data.toJSON());

    const rest = new REST({version: '9'}).setToken(process.env.DISCORD_TOKEN);
    rest.put(Routes.applicationGuildCommands(process.env.DISCORD_CLIENT_ID, process.env.DISCORD_GUILD_ID), {body: commands})
        .then(() => logger.info('Successfully registered application commands.'))
        .catch(e => logger.error(e));
};

export const removeCommands = () => {
    console.log('Cleaning up commands and exiting.')
    const commands = [];

    const rest = new REST({version: '9'}).setToken(process.env.DISCORD_TOKEN);
    rest.put(Routes.applicationGuildCommands(process.env.DISCORD_CLIENT_ID, process.env.DISCORD_GUILD_ID), {body: commands})
        .then(() => {
            logger.info('Successfully removed application commands.');
            process.exit(0);
        })
        .catch(e => logger.error(e));
};

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
export const getRandomArbitrary = (min, max) => {
    return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


export const chunkArray = (arr, chunkSize) => {
    return arr.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / chunkSize);

        // start a new chunk
        if (!resultArray[chunkIndex])
            resultArray[chunkIndex] = []


        resultArray[chunkIndex].push(item)
        return resultArray
    }, []);
}

export const ordinal = i => {
    let j = i % 10,
        k = i % 100;
    if (j === 1 && k !== 11) {
        return i + "st";
    }
    if (j === 2 && k !== 12) {
        return i + "nd";
    }
    if (j === 3 && k !== 13) {
        return i + "rd";
    }
    return i + "th";
}

export const chunkSubstr = (str, size) => {
    const numChunks = Math.ceil(str.length / size)
    const chunks = new Array(numChunks)

    for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
        chunks[i] = str.substr(o, size)
    }
    return chunks;
}




export const unhandledError = async (interaction, error) => {
    logger.error(error);
    try {

    if (interaction.replied || interaction.deferred)
        interaction.editReply({embeds: [embedReply.error('There was an unhandled error while executing this command!')], ephemeral: true}).then();
    else
        interaction.reply({embeds: [embedReply.error('There was an unhandled error while executing this command!')], ephemeral: true}).then();
    } catch (e) {
        logger.fatal('Unable to respond to interaction.  See below for more info.')
        logger.fatal(e)
    }
}
