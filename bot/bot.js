import {Client, Collection, Intents} from 'discord.js';
import {GeneralCog} from './cogs/general/index.js';
import {PeertubeCog} from './cogs/peertube/index.js';
import {RedditCog} from './cogs/reddit/index.js';
import registerOnReady from './events/ready.js';
import {dotEnv, logger, removeCommands} from './lib/utils.js';


export default async () => {
    // Create a new client instance
    const client = new Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS]});

    // Create commands collection on client
    client.commands = new Collection();
    client.services = new Collection();
    client.utils = new Collection();

    // Create list of cogs
    const cogs = [
        new RedditCog(client),
        new GeneralCog(client),
        new PeertubeCog(client),
    ]

    // Register commands from cogs
    for (let cog of cogs) cog.registerCommands().then();
    // Register events from cogs
    for (let cog of cogs) cog.registerEvents().then();

    // Register global events
    registerOnReady(client);

    const exitGracefully =() => {
        logger.info('Exiting Discord bot...');
        if (!dotEnv.DEV) removeCommands();
        process.exit(0)
    }

    process.on('SIGINT', exitGracefully);
    process.on('SIGTERM', exitGracefully)

    // Login to Discord with your client's token
    await client.login(dotEnv.DISCORD_TOKEN);
}

