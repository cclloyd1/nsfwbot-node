import {deployCommands, logger} from '../lib/utils.js';


const handleOnReady = async (client) => {
    logger.info(`Bot logged in with user ID ${client.user.id}`);
    deployCommands(client);
}


export default async (client) => {
    client.once('ready', (...args) => {
        handleOnReady(client)
    });
}
