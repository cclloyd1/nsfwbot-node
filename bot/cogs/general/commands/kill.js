import {SlashCommandBuilder} from '@discordjs/builders';
import { Permissions } from 'discord.js';
import {embedReply} from '../../../lib/enums.js';


const data = new SlashCommandBuilder()
    .setName('kill')
    .setDescription('Kills the bot so that it can automatically restart.')
    .addBooleanOption(input => input
        .setName('confirm')
        .setDescription('Confirm you actually want to kill the bot.')
    )
    //.setDefaultPermission(Permissions.FLAGS.MANAGE_GUILD)
    // TODO: Only allow manage_guild to kill bot


const handleKill = async (interaction) => {
    if (interaction.member.permissions.has(Permissions.FLAGS.MANAGE_GUILD)) {
        const confirm = interaction.options.getBoolean('confirm');
        if (!confirm) {
            await interaction.reply({embeds: [embedReply.warning('You must confirm that you want to kill the bot.')], ephemeral: true});
            return;
        }
        await interaction.reply({embeds: [embedReply.running('Attempting to kill bot...')]});
        process.kill(0);
    }
    else {
        await interaction.reply({embeds: [embedReply.error('You don\'t have permission to use that command.')], ephemeral: true});
    }
}

export default {
    data: data,
    execute: handleKill,
}
