import {SlashCommandBuilder} from '@discordjs/builders';
import {dotEnv, logger, removeCommands} from '../../../lib/utils.js';


const data = new SlashCommandBuilder()
    .setName('cleanup')
    .setDescription('Removes all commands from the guild and exits.')
    .addBooleanOption(input => input
        .setName('confirm')
        .setDescription('Confirm you actually want to kill the bot.')
    )
    //.setDefaultPermission(Permissions.FLAGS.MANAGE_GUILD)
    // TODO: Only allow manage_guild to kill bot


const handleCleanup = async (interaction) => {
    if (interaction.options.getBoolean('confirm')) {
        logger.info('Removing commands');
        removeCommands();
    }
}
export default {
    data: data,
    execute: handleCleanup,
}
