import {BaseCog} from '../../lib/BaseCog.js';
import cmdKill from './commands/kill.js';
import cmdCleanup from './commands/cleanup.js';
import interactionCreate from './events/interactionCreate.js';


export class GeneralCog extends BaseCog {

    constructor(client) {
        super(client);
    }

    registerCommands = async () => {
        this.client.commands.set('kill', cmdKill);
        this.client.commands.set('cleanup', cmdCleanup);
    }

    registerEvents = async () => {
        this.client.on('interactionCreate', (...args) => interactionCreate(...args));
    }
}
