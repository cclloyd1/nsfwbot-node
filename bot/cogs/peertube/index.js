import {BaseCog} from '../../lib/BaseCog.js';
import {dotEnv, logger} from '../../lib/utils.js';
import peertubeCommand from './commands/index.js';
import interactionCreate from './events/interactionCreate.js';


export class PeertubeCog extends BaseCog {

    constructor(client) {
        super(client);
    }

    registerCommands = async () => {
        // Check if any vars not set and exit early
        if ([
            dotEnv.PEERTUBE_HOST,
            dotEnv.PEERTUBE_USERNAME,
            dotEnv.PEERTUBE_PASSWORD,
            dotEnv.PEERTUBE_CHANNEL,
        ].includes(null)) {
            logger.warn('Not all Peertube environment variables set.  Not using PeerTube commands.');
            return;
        }

        // Register commands
        this.client.commands.set('peertube', peertubeCommand);
    }

    registerEvents = async () => {
        this.client.on('interactionCreate', (...args) => interactionCreate(...args));
    }
}

