import {MessageEmbed} from 'discord.js';
import {embedReply} from '../../../lib/enums.js';
import axios from "axios";
import qs from "qs";
import FormData from "form-data";
import { extract } from 'oembed-parser'
import YTDlpWrap from 'yt-dlp-wrap';
import {dotEnv, logger} from '../../../lib/utils.js';


export const importCommand = subcommand => subcommand
    .setName('import')
    .setDescription('Import video from URL into a Peertube instance')
    .addStringOption(input => input
        .setName('url')
        .setDescription('The URL of the video to import (compatible with yt-dlp)')
        .setRequired(true)
    )


export const handleImportCommand = async (interaction) => {
    await interaction.deferReply({ephemeral: true});
    //const url = 'https://www.xvideos.com/video11416121/sweet_ass_casey_calvert_riding_cock_on_the_sofa';
    const url = interaction.options.getString('url');

    let res;


    // Get video info

    let videoData;
    //try {
    //    videoData = await extract(url);
    //} catch (error) {
    //    logger.error(error);
    //    await interaction.editReply({embeds: [embedReply.error(`Error fetching video info for URL.`)], ephemeral: true });
    //    return;
    //}
    const ytdlp = YTDlpWrap.default;
    const ytDlpWrap = new ytdlp(dotEnv.YTDLP_PATH);
    const version = await ytDlpWrap.getVersion()

    const metadata = await ytDlpWrap.getVideoInfo(url);

    try {
        res = await axios.get(`https://${dotEnv.PEERTUBE_HOST}/api/v1/oauth-clients/local`)
    } catch (error) {
        logger.error(error)
        await interaction.editReply({embeds: [embedReply.error(`Error fetching client info for ${dotEnv.PEERTUBE_HOST}`)], ephemeral: true });
        return;
    }

    const tokenData = {
        client_id: res.data.client_id,
        client_secret: res.data.client_secret,
        grant_type: 'password',
        username: dotEnv.PEERTUBE_USERNAME,
        password: dotEnv.PEERTUBE_PASSWORD,
    }

    try {
        res = await axios({
            url: `https://${dotEnv.PEERTUBE_HOST}/api/v1/users/token`,
            method: 'POST',
            data: qs.stringify(tokenData),
            headers: {'Content-Type': `application/x-www-form-urlencoded`}
        });
    } catch (error) {
        logger.error(error)
        await interaction.editReply({embeds: [embedReply.error(`Error fetching token for ${dotEnv.PEERTUBE_HOST}`)], ephemeral: true });
        return;
    }
    const auth = res.data;
    logger.info('Auth', auth);

    // Get video title
    const options = {
        // All peertube request options such as privacy, wait_transcoding, etc
        channelId: dotEnv.PEERTUBE_CHANNEL,
        name: metadata.title,
        targetUrl: url,
        privacy: 4,
        waitTranscoding: true,
    }
    // Attach token to request

    try {
        logger.info(`Uploading '${metadata.title}' to ${dotEnv.PEERTUBE_HOST}`)
        res = await axios({
            url: `https://${dotEnv.PEERTUBE_HOST}/api/v1/videos/imports`,
            method: 'POST',
            data: options,
            headers: {
                //'Content-Type': `application/x-www-form-urlencoded`,
                Authorization: `${auth.token_type} ${auth.access_token}`
            }
        });
    } catch (error) {
        logger.error(error)
        await interaction.editReply({embeds: [embedReply.error(`Error uploading video to ${dotEnv.PEERTUBE_HOST}`)], ephemeral: true });
        return;
    }

    await interaction.editReply({embeds: [embedReply.success(`Completed importing video into ${dotEnv.PEERTUBE_HOST}`)], ephemeral: true })
}

