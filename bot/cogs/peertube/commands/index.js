import {SlashCommandBuilder} from '@discordjs/builders';
import {handleImportCommand, importCommand} from './import.js';


const data = new SlashCommandBuilder()
    .setName('peertube')
    .setDescription('Manage PeerTube videos')
    .addSubcommand(subcommand => importCommand(subcommand))


const executePeertubeCommand = async (interaction) => {
    switch (interaction.options.getSubcommand()) {
        case 'import': await handleImportCommand(interaction); break;
        default: break;
    }
}

export default {
    data: data,
    execute: executePeertubeCommand,
}
