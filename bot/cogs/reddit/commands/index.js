import {SlashCommandBuilder} from '@discordjs/builders';
import {cleanCommand, handleCleanCommand} from './clean.js';
import {deflowerCommand, handleDeflowerCommand} from './deflower.js';
import {handleMultiCommand, multiCommand} from './multi.js';
import {
    cmdRedditConfig,
    cmdRedditRestart,
    cmdRedditStart,
    cmdRedditStatus,
    cmdRedditStop, handleRedditConfig,
    handleRedditRestart,
    handleRedditStart, handleRedditStatus,
    handleRedditStop,
} from './service.js';
import {handleTransferCommand, transferCommand} from './transfer.js';


const data = new SlashCommandBuilder()
    .setName('reddit')
    .setDescription('Manages reddit accounts')
    .addSubcommand(subcommand => transferCommand(subcommand))
    .addSubcommand(subcommand => cleanCommand(subcommand))
    .addSubcommand(subcommand => deflowerCommand(subcommand))
    .addSubcommand(subcommand => multiCommand(subcommand))
    .addSubcommandGroup(group => group
        .setName('service')
        .setDescription('Manage the reddit service')
        .addSubcommand(subcommand => cmdRedditStart(subcommand))
        .addSubcommand(subcommand => cmdRedditStop(subcommand))
        .addSubcommand(subcommand => cmdRedditRestart(subcommand))
        .addSubcommand(subcommand => cmdRedditStatus(subcommand))
        .addSubcommand(subcommand => cmdRedditConfig(subcommand))
    )


const executeRedditCommand = async (interaction) => {
    switch (interaction.options.getSubcommand()) {
        case 'transfer': await handleTransferCommand(interaction); break;
        case 'clean': await handleCleanCommand(interaction); break;
        case 'deflower': await handleDeflowerCommand(interaction); break;
        case 'multi': await handleMultiCommand(interaction); break;
        case 'start': await handleRedditStart(interaction); break;
        case 'stop': await handleRedditStop(interaction); break;
        case 'restart': await handleRedditRestart(interaction); break;
        case 'status': await handleRedditStatus(interaction); break;
        case 'config': await handleRedditConfig(interaction); break;
        default: break;
    }
}

export default {
    data: data,
    execute: executeRedditCommand,
}
