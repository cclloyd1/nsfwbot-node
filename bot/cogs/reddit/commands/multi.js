import {embedReply} from '../../../lib/enums.js';
import {ra, rn} from "../../../lib/reddit.js";
import {dotEnv, logger} from '../../../lib/utils.js';


export const multiCommand = subcommand => subcommand
    .setName('multi')
    .setDescription('Copy all NSFW subreddits on main to the multireddit.')


export const handleMultiCommand = async (interaction) => {
    await interaction.deferReply({ephemeral: true});
    logger.info('Fetching saved posts on main');
    const nsfw = (await rn.getSubscriptions({limit: 200}))//.filter(s => s.over18);
    logger.info(`Found ${nsfw.length} nsfw subreddits on main account`);

    // Find NSFW multireddit on main account
    const multis = (await rn.getMyMultireddits()).filter(m => m.display_name.toLowerCase() === 'nsfw');
    const multi = multis.length > 0 ? multis[0] : null;

    // Exit early if no multireddit found
    if (!multi) {
        await interaction.editReply({embeds: [embedReply.warning('Multireddit named \'NSFW\' not found on main account')], ephemeral: true })
        return;
    }

    for (let s of nsfw) {
        logger.info(`Adding ${s.display_name_prefixed} to multireddit on main.`)
        if (!dotEnv.DRYRUN) await multi.addSubreddit(s);
    }

    await interaction.editReply({embeds: [embedReply.success('Completed adding NSFW subreddits from main to multireddit')], ephemeral: true })
}

