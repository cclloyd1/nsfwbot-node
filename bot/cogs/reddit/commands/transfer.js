import {MessageEmbed} from 'discord.js';
import {embedReply} from '../../../lib/enums.js';
import {ra, rn} from "../../../lib/reddit.js";
import {chunkSubstr, dotEnv, logger} from '../../../lib/utils.js';


export const transferCommand = subcommand => subcommand
    .setName('transfer')
    .setDescription('Transfer saved nsfw posts from main to alt account')
    .addBooleanOption(input => input
        .setName('includesafe')
        .setDescription('Also transfers nsfw posts from regular, non-nsfw subreddits')
    )


const handleRedgifs = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase())

    // TODO: get subreddit icon and post that as user and avatar instead of nsfwbot

    let embed = new MessageEmbed()
        .setTitle(submission.title)
        .setDescription(submission.url)
        .setURL(`https://reddit.com${submission.permalink}`)
        .setFooter({text: submission.id})
    if (submission.media?.oembed?.thumbnail_url)
        embed.setThumbnail(submission.media.oembed.thumbnail_url)
    await channel.send({
        embeds: [embed],
    });
}


const handleImgur = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase())

    let embed = new MessageEmbed()
        .setTitle(submission.title)
        .setDescription(submission.url)
        .setURL(`https://reddit.com${submission.permalink}`)
        .setThumbnail(submission.preview.images[0].resolutions.slice(-1)[0].url)
        .setFooter({text: submission.id})
    await channel.send({
        embeds: [embed],
    });
}


const handleComment = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase())
    const matches = submission.permalink.match(/comments\/([A-Za-z0-9_]+)\//);
    const parentId = matches ? matches[1] : undefined;
    const parent = await rn.getSubmission(parentId).fetch();

    // Chunk message into sections of 4096 (maximum embed description length)
    const chunks = chunkSubstr(`${submission.link_permalink}\n\n${submission.body}`.replace(/[\r\n]{2,}/g, '\n'), 4096);
    for (let chunk of chunks) {
        let embed = new MessageEmbed()
            .setTitle(parent.title)
            .setDescription(chunk)
            .setURL(`https://reddit.com${submission.permalink}`)
            .setFooter({text: submission.id})
        await channel.send({embeds: [embed]});
    }
}


const handleText = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase());

    // Trim excess whitespace
    submission.selftext = submission.selftext.replace(/(\r\n|\r|\n){2,}/g, '$1\n');

    // Chunk message into sections of 4096 (maximum embed description length)
    const chunks = chunkSubstr(submission.selftext.replace(/[\r\n]{2,}/g, '\n'), 4096);
    for (let chunk of chunks) {
        let embed = new MessageEmbed()
            .setTitle(submission.title)
            .setDescription(chunk)
            .setURL(`https://reddit.com${submission.permalink}`)
            .setFooter({text: submission.id})
        await channel.send({embeds: [embed]});
    }

}

const handleRedditImage = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase());

    let embed = new MessageEmbed()
        .setTitle(submission.title)
        .setURL(`https://reddit.com${submission.permalink}`)
        .setImage(submission.url)
        .setFooter({text: submission.id})
    await channel.send({
        embeds: [embed],
    });

}

const handleDefault = async (client, submission) => {
    const guild = await client.guilds.cache.first();

    // Get channel to post in
    let channel = guild.channels.cache.find(channel => channel.type === 'GUILD_TEXT' && channel.name.toLowerCase() === submission.subreddit.display_name.toLowerCase());
    // Create channel if not exists
    if (!channel) channel = await guild.channels.create(submission.subreddit.display_name.toLowerCase())

    submission.all_awardings = [];
    logger.info(submission);
    await channel.send({content: 'Printed submission to console', ephemeral: true});
}


export const transferPosts = async (client, safe=false) => {
    logger.info('Fetching saved posts');
    const nsfw = (await rn.getUser(dotEnv.REDDIT_MAIN_USERNAME).getSavedContent({limit: 2000})).filter(s => s.over_18);
    logger.info(`Found ${nsfw.length} nsfw posts on main account`);

    for (let s of nsfw) {
        // Get subreddit of submission
        const sub = await rn.getSubreddit(s.subreddit.display_name).fetch();

        // Exit if nsfw post in regular sub
        if (sub.over18 === false && safe === false) continue;
        // Save the post on alt account
        if (!dotEnv.DRYRUN) await ra.getSubmission(s.id).save();

        // TODO: process reddit video
        if (s.is_video) continue;

        // Arbitrarily add type to submission when none found
        if (s.url && s.url.includes('imgur')) s.media = {type: 'imgur'}
        else if (s.constructor.name === 'Comment') s.media = {type: 'comment'}
        else if (s.selftext) s.media = {type: 'text'}
        else if (s.url && s.url.includes('i.redd.it')) s.media = {type: 'reddit_image'}
        else if (s.url && s.url.includes('gfycat')) {
            s.media = { type: 'redgifs.com' };
            s.url = s.url.replace('gfycat.com', 'www.redgifs.com').toLowerCase();
        }

        let foundHandler = true;

        // Handle replying to the interaction using appropriate handler
        try {
            switch (s.media?.type) {
                case 'redgifs.com':
                    await handleRedgifs(client, s, safe);
                    break;
                case 'imgur':
                    await handleImgur(client, s, safe);
                    break;
                case 'comment':
                    await handleComment(client, s, safe);
                    break;
                case 'text':
                    await handleText(client, s, safe);
                    break;
                case 'reddit_image':
                    await handleRedditImage(client, s, safe);
                    break;
                default:
                    logger.info('Using default media handler');
                    await handleDefault(client, s, safe);
                    foundHandler = false;
                    break;
            }
        } catch (error) {
            // Assume the post wasn't handled properly, so don't unsave yet.
            logger.error(error);
            foundHandler = false;
        }

        // Unsave post on main account (except dry runs)
        if (!dotEnv.DRYRUN && foundHandler) {
            logger.info(`Unsaving (${s.id}): ${s.title ?? ''} [${s.subreddit_name_prefixed}]`);
            if (!dotEnv.DRYRUN) await s.unsave();
        }
        else {
            logger.info(`SKIPPING: Unsaving (${s.id}): ${s.title ?? ''} [${s.subreddit_name_prefixed}]`);
        }
        //break;
    }
}


export const handleTransferCommand = async (interaction) => {
    await interaction.deferReply({ephemeral: true});
    const safe = interaction?.options?.getBoolean('includesafe') ?? false
    await transferPosts(interaction.client, safe);
    await interaction.editReply({embeds: [embedReply.success('Completed moving saved posts from main to alt account.')], ephemeral: true })
}

