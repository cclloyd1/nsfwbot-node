import {embedReply} from '../../../lib/enums.js';
import {ra, rn} from "../../../lib/reddit.js";
import {dotEnv, logger} from '../../../lib/utils.js';


export const deflowerCommand = subcommand => subcommand
    .setName('deflower')
    .setDescription('Moves all NSFW subreddit subscriptions to main account.')
    .addBooleanOption(input => input
        .setName('unsubscribe')
        .setDescription('Unsubscribe from alt account when transferring')
    )


export const handleDeflowerCommand = async (interaction) => {
    const unsubscribe = interaction.options.getBoolean('unsubscribe') ?? false;
    await interaction.deferReply({ephemeral: true});

    // Get all NSFW subreddits
    logger.info('Fetching subscriptions on alt');
    const nsfw = (await ra.getSubscriptions({limit: 200})).filter(s => s.over18);
    logger.info(`Found ${nsfw.length} nsfw subreddits on alt account`);

    for (let nsfw_s of nsfw) {
        logger.info(`Transferring from alt to main: ${nsfw_s.display_name_prefixed}`)
        // Get subreddit on main account
        const s = rn.getSubreddit(nsfw_s.display_name);

        // Transfer subscription
        if (!dotEnv.DRYRUN) {
            if (unsubscribe) await nsfw_s.unsubscribe();
            await s.subscribe();
        }
    }

    await interaction.editReply({embeds: [embedReply.success('Completed moving subreddits from main to alt account.')], ephemeral: true })
}

