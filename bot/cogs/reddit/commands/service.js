import {embedReply, serviceStatus, serviceStatusName} from '../../../lib/enums.js';
import {logger} from '../../../lib/utils.js';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration.js';
import {RedditService} from '../service.js';
dayjs.extend(duration)


export const cmdRedditStatus = subcommand => subcommand
    .setName('status')
    .setDescription('Get the reddit service status.')

export const cmdRedditStart = subcommand => subcommand
    .setName('start')
    .setDescription('Start the reddit service.')
    .addBooleanOption(option => option
        .setName('force')
        .setDescription('Start even if already running')
    )


export const cmdRedditStop = subcommand => subcommand
    .setName('stop')
    .setDescription('Stop the reddit service.')


export const cmdRedditRestart = subcommand => subcommand
    .setName('restart')
    .setDescription('Restart the reddit service.')


export const cmdRedditConfig = subcommand => subcommand
    .setName('config')
    .setDescription('Modify the configuration for the reddit service')
    .addIntegerOption(option => option
        .setName('interval')
        .setDescription('Minutes between time when the service runs. (Default 15m)')
    )
    .addBooleanOption(option => option
        .setName('notifications')
        .setDescription('Send notifications to specified channel.')
    )


export const handleRedditStart = async (interaction) => {
    const force = interaction.options.getBoolean('force');
    let service = interaction.client.services.get('reddit');

    // Create service if not exists
    if (!service) service = new RedditService(interaction.client);

    if (service.status !== serviceStatus.RUNNING) {
        await service.start();
        await interaction.editReply({embeds: [embedReply.success('Started reddit service.')]});
    }
}


export const handleRedditStop = async (interaction) => {
    const service = interaction.client.services.get('reddit');

    if (service) {
        await service.stop();
        await interaction.editReply({embeds: [embedReply.success('Stopped reddit service.')]});
    }
}


export const handleRedditRestart = async (interaction) => {
    const service = interaction.client.services.get('reddit');

    if (service) {
        await service.restart();
        await interaction.editReply({embeds: [embedReply.success('NOT IMPLEMENTED')]});
    }
}

export const handleRedditStatus = async (interaction) => {
    const service = interaction.client.services.get('reddit');
    await service.notify(interaction);
}


export const handleRedditConfig = async (interaction) => {
    const interval = interaction.options.getInteger('interval');
    const notifications = interaction.options.getBoolean('notifications');
    const service = interaction.client.services.get('reddit');

    const embed = embedReply.success('Updated reddit service.');

    if (interval) {
        service.interval = dayjs.duration({minutes: interval});
        logger.info(`Changed reddit service interval to ${service.interval.asMilliseconds()} (${service.interval.asMinutes()} minutes)`)
        embed.addField('Interval', `${interval}`, true);
    }
    service.notifications = notifications === true;

    await interaction.editReply({embeds: [embedReply.success('Modified reddit service.')]});
}

