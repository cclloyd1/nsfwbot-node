import {embedReply} from '../../../lib/enums.js';
import {ra, rn} from "../../../lib/reddit.js";
import {dotEnv, logger} from '../../../lib/utils.js';


export const cleanCommand = subcommand => subcommand
    .setName('clean')
    .setDescription('Moves all NSFW subreddit subscriptions to alt account.')
    .addBooleanOption(input => input
        .setName('skipmulti')
        .setDescription('Do not store transferred subreddits in NSFW multireddit on main account')
    )


export const handleCleanCommand = async (interaction) => {
    const skipmulti = interaction.options.getBoolean('skipmulti') ?? false;
    await interaction.deferReply({ephemeral: true});

    // Get all subreddits on main
    logger.info('Fetching subscriptions on main');
    const nsfw = (await rn.getSubscriptions({limit: 200})).filter(s => s.over18);
    logger.info(`Found ${nsfw.length} nsfw subreddits on main account`);

    // Find NSFW multireddit on main account
    const multis = (await rn.getMyMultireddits()).filter(m => m.display_name.toLowerCase() === 'nsfw');
    const multi = multis.length > 0 ? multis[0] : null;

    for (let s of nsfw) {
        logger.info(`Transferring from main to alt: ${s.display_name_prefixed}`)
        // Get subreddit on alt account
        const nsfw_s = ra.getSubreddit(s.display_name);

        // Add subreddits to multireddit on main account.
        if (multi && !skipmulti) {
            logger.info(`Adding ${s.display_name_prefixed} to multireddit on main.`);
            if (!dotEnv.DRYRUN) await multi.addSubreddit(s);
        }

        // Transfer subscription
        if (!dotEnv.DRYRUN) {
            await nsfw_s.subscribe();
            await s.unsubscribe();
        }
    }

    await interaction.editReply({embeds: [embedReply.success('Completed moving subreddits from main to alt account.')], ephemeral: true })
}

