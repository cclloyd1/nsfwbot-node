import {BaseService} from '../../lib/BaseService.js';
import {dotEnv, logger} from '../../lib/utils.js';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration.js';
import {transferPosts} from './commands/transfer.js';
dayjs.extend(duration)


export class RedditService extends BaseService {
    constructor(client, options={}) {
        super(client, {
            ...options,
            name: 'reddit',
            enabled: dotEnv.REDDIT_SERVICE,
            interval: options.interval ?? 15,
        });
    }

    async handle() {
        await transferPosts(this.client, false);
    }

}
