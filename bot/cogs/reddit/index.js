import {BaseCog} from '../../lib/BaseCog.js';
import redditCommand from './commands/index.js';
import interactionCreate from './events/interactionCreate.js';
import ready from './events/ready.js';


export class RedditCog extends BaseCog {

    constructor(client) {
        super(client);
    }

    registerCommands = async () => {
        this.client.commands.set('reddit', redditCommand);
    }

    registerEvents = async () => {
        this.client.on('interactionCreate', (...args) => interactionCreate(...args));
        this.client.on('ready', (...args) => ready(...args));
    }
}

