import {logger, unhandledError} from '../../../lib/utils.js';


export default async (interaction) => {
    if (interaction.isCommand()) {
        const command = interaction.client.commands.get(interaction.commandName);
        if (!command) return;
        try {
            // Return if command not found, skip (continue) if found
            switch (command.data.name) {
                case 'reddit': break;
                default: return;
            }
            // Execute command
            await command.execute(interaction);
        } catch (error) {
            await unhandledError(interaction, error);
        }
    }
}
