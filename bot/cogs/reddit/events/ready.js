import dayjs from 'dayjs';
import {RedditService} from '../service.js';
import duration from 'dayjs/plugin/duration.js';
dayjs.extend(duration)


export default async (client) => {
    const service = new RedditService(client, {});
    if (service.enabled) await service.start();
}
