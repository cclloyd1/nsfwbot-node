export default {
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
    },

    env: {
        browser: false,
        es6: true,
    },

    extends: [
        'eslint:recommended',
        'plugin:prettier/recommended'
    ],

    plugins: [],

    rules: {
        'no-unused-vars': 'warn',
        'prettier/prettier': [
            'error',
            {
                endOfLine: 'auto',
            },
        ],
        'no-shadow': ['error', {builtinGlobals: true, hoist: 'all', allow: ['event']}],
        'no-var': 'error',
    },
    overrides: [
        {
            files: ['./*.js'],
            env: {
                node: true,
            },
        },
    ],
}
